﻿using Newtonsoft.Json;
using PolygonOnlinePostSTLExample.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PolygonOnlinePostSTLExample
{
	public class Program
	{
        static readonly Uri ApiBaseUri = new Uri("https://polygon-online.ru/");


		public static void Main(string[] args)
		{
            try
            {
                Task.Run(AuthAndUploadFile_Example)
                    .Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception:");
                Console.WriteLine($"* Message: {ex.Message}");
                Console.WriteLine($"* StackTrace: {ex.StackTrace}");
            }

            Console.ReadLine();
        }

        public static async void AuthAndUploadFile_Example()
        {
            var login = "login";
            var password = "pass";

            // Handler with cookie container
            using (var handler = new HttpClientHandler())
            {
                // (1) Authorize request example
                // !!! fetch auth cookies to handler
                var userInfoData = await HTTP_POST_Authorize(handler, login, password);

                if (userInfoData?.UserInfo.IsRegistered != true)
                {
                    throw new ApplicationException("Unexpected unregistered");
                }

                Debugger.Break();

                // (2) Upload stl file example
                // !!! use auth cookies from handler to all other requests
                using (var fileStream = new MemoryStream(Properties.Resources.testModel))
                {
                    var uploadResponse = await HTTP_POST_UploadSTLFile(handler, fileStream, "testModel.stl");

                    if (uploadResponse.Status != GeneralApiResponse<FileHeaderDto>.ResponseStatus.Success)
                    {
                        throw new ApplicationException($"Unexpected errors: {string.Join(", ", uploadResponse.Errors ?? new string[0])}");
                    }

                    Debugger.Break();
                }
            }
        }


        /// <summary>
        /// Check session details
        /// </summary>
        public static async Task<UserInfoResponse> HTTP_GET_GetUserInfo(HttpClientHandler handler)
        {
            using (var httpClient = new HttpClient(handler, false))
            {
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

                try
                {
                    var response = await httpClient.GetAsync(new Uri(ApiBaseUri, "/api/UserAccounts/GetUserInfo"));

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new ApplicationException($"GetUserInfo StatusCode = '{response.StatusCode}'");
                    }

                    var responseJsonSting = await response.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject<UserInfoResponse>(responseJsonSting);

                    return result;
                }
                catch
                {
                    throw;
                }
            }
        }


        public static async Task<UserInfoResponse> HTTP_POST_Authorize(HttpClientHandler handler, string login, string password)
        {
            using (var httpClient = new HttpClient(handler, false))
            {
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

                var requestObject = new { Login = login, Password = password };
                var requestJsonString = JsonConvert.SerializeObject(requestObject);

                var request = new StringContent(requestJsonString, Encoding.UTF8, "application/json");

                try
                {
                    var response = await httpClient.PostAsync(new Uri(ApiBaseUri, "/api/UserAccounts/AuthorizeShortSession"), request);

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new ApplicationException($"Authorize StatusCode = '{response.StatusCode}'");
                    }

                    var responseJsonSting = await response.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject<UserInfoResponse>(responseJsonSting);

                    return result;
                }
                catch
                {
                    throw;
                }
            }
        }


        public static async Task<GeneralApiResponse<FileHeaderDto>> HTTP_POST_UploadSTLFile(HttpClientHandler handler, Stream fileStream, string fileName)
        {
            using (var httpClient = new HttpClient(handler, false))
            {
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

                using (var request = new MultipartFormDataContent())
                {
                    request.Add(new StreamContent(fileStream), "File", fileName);

                    try
                    {
                        var response = await httpClient.PostAsync(new Uri(ApiBaseUri, "/api/folder/upload"), request);

                        var responseJsonSting = await response.Content.ReadAsStringAsync();

                        var result = JsonConvert.DeserializeObject<GeneralApiResponse<FileHeaderDto>>(responseJsonSting);

                        return result;
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
        }
    }
}
