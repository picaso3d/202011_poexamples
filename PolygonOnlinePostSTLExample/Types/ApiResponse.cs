﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonOnlinePostSTLExample.Types
{
	public sealed class GeneralApiResponse<T>
		where T: class
	{
		/// <summary>
		/// Статус(успешно ли выполнена команда)
		/// </summary>
		public ResponseStatus Status { get; set; }

		/// <summary>
		/// Сообщения об ошибке (если есть)
		/// </summary>
		public string[] Errors { get; set; }

		/// <summary>
		/// Данные
		/// </summary>
		public T Data { get; set; }

		public enum ResponseStatus
		{
			Failed = 0,
			Success
		}
	}
}
