﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonOnlinePostSTLExample.Types
{
	public class FileHeaderDto
	{
		/// <summary>
		/// Идентификатор записи
		/// </summary>
		public Guid Id { get; set; }

		/// <summary>
		/// Дата сохранения записи в базу
		/// </summary>
		public DateTime CDateUtc { get; set; }

		/// <summary>
		/// Дата обновления записи в базе
		/// </summary>
		public DateTime UDateUtc { get; set; }

		/// <summary>
		/// Название файла
		/// </summary>
		public string FileName { get; set; }

		/// <summary>
		/// Размер файла
		/// </summary>
		public UInt32 FileSize { get; set; }
	}
}
