﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonOnlinePostSTLExample.Types
{
	public class UserInfoResponse
	{
		public UserInfoContract UserInfo { get; set; }
	}

	public class UserInfoContract
	{
		/// <summary>
		/// Идентификатор пользователя во внутренних системах
		/// </summary>
		public Guid Id { get; set; }

		/// <summary>
		/// Полное наименование пользователя
		/// </summary>
		public string FullName { get; set; }

		/// <summary>
		/// IsRegistered
		/// </summary>
		public bool IsRegistered { get; set; }

		/// <summary>
		/// Пользовательские роли, доступа к разным разделам портала
		/// </summary>
		public CustomerRole[] CustomerRoles { get; set; }

		/// <summary>
		/// Роль/зона по умолчанию
		/// </summary>
		public CustomerRole CustomerDefault { get; set; }

		/// <summary>
		/// Типы клиентов настроенные в аккаунте
		/// </summary>
		public ClientType[] ClientTypes { get; set; }

		/// <summary>
		/// Уровень пользовательских настроек
		/// </summary>
		public ConfigLevel ConfigLevel { get; set; }
	}

	public enum CustomerRole
	{
		None = 0, 
		SourceFolder, 
		PolygonWeb, 
		ConfirmCurator, 
		CuratorChildren, 
		CuratorDistribution, 
		ConfirmExecutor, 
		ExecutorEmployee, 
		ExecutorDistribution, 
		ExecutorEquipment,
		ExecutorLogistic
	}

	public enum ClientType
	{
		Unknown = 0,
		Operator,
		Curator,
		CuratorChild,
		Executor
	}

	public enum ConfigLevel
	{
		None,
		Basic, 
		Extended
	}

}
